package methods;

import utilities.RandomNumberGenerator;

public class RandomNumberPractices {
    public static void main(String[] args) {

        /*
        Generate 5 random numbers bt 5 and 20 and find their average
         */

        int r1 = RandomNumberGenerator.getARandomNumber(5,20);
        int r2 = RandomNumberGenerator.getARandomNumber(5,20);
        int r3 = RandomNumberGenerator.getARandomNumber(5,20);
        int r4 = RandomNumberGenerator.getARandomNumber(5,20);

        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
        System.out.println(r4);

        System.out.println("Avarage is : " + (r1 + r2 + r3 + r4)/ 4);



    }


}

package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class _03_IntegerArrayList {
    public static void main(String[] args) {

        /*
        Create an ArrayList to store below numbers
        10
        15
        20
        10
        20
        30
         */
        System.out.println("\n-----TASK01-----");

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(0,10);
        numbers.add(1,15);
        numbers.add(2,20);
        numbers.add(3,10);
        numbers.add(4,20);
        numbers.add(5, 30);

        System.out.println(numbers);
        System.out.println(numbers.size());

//         while(numbers.contains(10)){
//             numbers.remove((Integer)10 );
//         }

        System.out.println("\n----contains() method ----\n");
        System.out.println(numbers.contains(10));
        System.out.println(numbers.contains(12));
        System.out.println(numbers.contains(20));
        System.out.println(numbers.contains(22));


        System.out.println("\n----indexOf() method ----\n");

        System.out.println(numbers.indexOf(15)); //1
        System.out.println(numbers.indexOf(25)); //-1
        System.out.println(numbers.indexOf(20));// 2

        System.out.println("\n----lastIndexOf() method ----\n");
        System.out.println(numbers.lastIndexOf(30));//5
        System.out.println(numbers.lastIndexOf(35));//-1
        System.out.println(numbers.lastIndexOf(10));//3

        System.out.println("\n ----Retrieving each element with for i loop-----\n");
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }


        System.out.println("\n ----Retrieving each element with for each loop-----\n");

        for(Integer number :numbers){
            System.out.println(number);
        }
    }
}

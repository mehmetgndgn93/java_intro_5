package loops.fori_loops;


import utilities.MathHelper;
import utilities.ScannerHelper;

public class Exercise05_PrintOddNumbersUsingScanner {
    public static void main(String[] args) {

//
//        int number = ScannerHelper.getANumber();
//        int i = ScannerHelper.getANumber();
//
//        for (int i = 0; i <= number; i--) {
//            if (i % 2 == 1) System.out.println(i);
//        }

        int num1 = ScannerHelper.getANumber();
        int num2 = ScannerHelper.getANumber();


        for (int i = Math.min(num1,num2); i <=Math.max(num1,num2) ; i++) {
            if (i % 2 == 0) System.out.println(i);

        }



    }
}

package loops.while_loops;

public class Exercise01_PrintOddNumbers {
    public static void main(String[] args) {
        /*
        Print odd numbers from 1 to 10 (both included) using while loop
         */

        int start = 1;

        while (start <= 10) {
            if (start % 2 != 0 )System.out.println(start);
            start++;

        }


    }
}

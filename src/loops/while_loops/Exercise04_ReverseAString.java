package loops.while_loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise04_ReverseAString {
    public static void main(String[] args) {

        String name = ScannerHelper.getAName();
        String reverseName ="";

        for (int i = name.length()-1; i >= 0 ; i--){
            reverseName += name.charAt(i);

        }
        System.out.println("The reversed name is = " + reverseName);


    }
}

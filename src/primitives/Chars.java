package primitives;

public class Chars {
    public static void main(String[] args) {
        /*
        Primitive char is used to store as sigle characater
        a-z
        A-Z
        0-9
        specials

         */

        char space = ' ';
        char dollarSign = '$';
        char percantageSign = '%';
        char upperCaseA = 'A';
        char favChar = '#';
        char firstCharInMyName = 'J';

        System.out.println(upperCaseA);
        System.out.println(favChar);
    }


}

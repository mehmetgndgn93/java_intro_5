package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("===========TASK1============");


        String myName = "Mehmet";
        System.out.println("My name is " + myName);


        System.out.println("===========TASK2============");

        char firstLetter = 'M';
        char secondLetter = 'e';
        char thirdLetter = 'h';
        char fourthLetter = 'm';
        char fifthLetter = 'e';
        char sixthLetter = 't';

        System.out.println("Starts here: " + firstLetter);
        System.out.println("============>" + secondLetter);
        System.out.println("============>" + thirdLetter);
        System.out.println("============>" + fourthLetter);
        System.out.println("============>" + fifthLetter);
        System.out.println("ends here==>:" + sixthLetter);


        System.out.println("===========TASK3============");


        String myFavMovie = "Clock Work Orange";
        String myFavSong = "Muse - Hysteria";
        String myFavCity = "Seattle";
        String myFavActivity = "Gym";
        String myFavSnack = "Raki";


        System.out.println("My favorite movie is: " + myFavMovie);
        System.out.println("My favorite snack is: " + myFavSong);
        System.out.println("My favorite city is: " + myFavCity);
        System.out.println("My favorite activity is: " + myFavActivity);
        System.out.println("My favorite snack is: " + myFavSnack);


        System.out.println("===========TASK4============");


        int myFavNumber = 777;
        int numberOfStatesIVisited = 5;
        int numberOfCountriesIVisited = 3;

        System.out.println("My favorite number is: \"" + myFavNumber + "\"");
        System.out.println("Number of the cities I have visited: \"" + numberOfStatesIVisited + "\"");
        System.out.println("Number of the countries I have visited: \"" + numberOfCountriesIVisited + "\"");


        System.out.println("===========TASK5============");


        boolean amIAtSchoolToday = false;

        System.out.println("I am at school today = " + amIAtSchoolToday);


        System.out.println("===========TASK6============");


        // I couldn`t figure out this one and couldn`t find the online lesson for this in canvas.
        // I will make sure to watch the recap/solution for this.


    }
}

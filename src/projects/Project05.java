package projects;

import java.util.Arrays;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n------TASK-1------\n");
        int[] numbers = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(numbers);

        System.out.println("\n------TASK-2------\n");
        int[] numberss = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallest(numberss);

        System.out.println("\n------TASK-4------\n");
        int[] numbers1 = {10, 5, 6, 7, 8, 5, 15, 15};

        findSecondGreatestAndSmallest(numbers1);


        System.out.println("\n------TASK-5------\n");
        String[] str = {"foo", "bar", "Foo, “bar", "6", "abc", "6", "xyz"};
        findDuplicates(str);

        System.out.println("------TASK-6-" + "-----");
        String[] str2 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(str2);

    }

    //("------TASK-1------");
    public static void findGreatestAndSmallestWithSort(int[] numbers) {

        Arrays.sort(numbers);
        if (numbers.length > 0) {
            System.out.println("Smallest = " + numbers[0]);
            System.out.println("Greatest = " + numbers[numbers.length - 1]);

        } else {
            System.out.println(" ");
        }
    }
    //("------TASK-2------");

    public static void findGreatestAndSmallest(int[] numbers) {
        int max = Integer.MIN_VALUE; // -1231232324
        int min = Integer.MAX_VALUE;

        for (int n : numbers) {
            if (max < n) {
                max = n;
            }

            if (min > n) {
                min = n;
            }
        }

        if (numbers.length > 0) {
            System.out.println("Max is = " + max);
            System.out.println("Min is = " + min);
        } else {
            System.out.println(" ");
        }
    }

    //("------TASK-4------");
    public static int findMax(int[] numbers) {
        int max = Integer.MIN_VALUE;

        for (int n : numbers) {
            if (n > max) max = n;
        }
        return max;
    }

    public static int findMin(int[] numbers) {
        int min = Integer.MAX_VALUE;

        for (int n : numbers) {
            if (n < min) min = n;
        }
        return min;
    }

    public static void findSecondGreatestAndSmallest(int[] numbers) {
        int secondMax = Integer.MIN_VALUE, secondMin = Integer.MAX_VALUE;
        for (int n : numbers) {
            if (n > secondMax && n < findMax(numbers)) {
                secondMax = n;
            }
            if (n < secondMin && n > findMin(numbers)) {
                secondMin = n;
            }
        }
        System.out.println("Second Smallest= " + secondMin);
        System.out.println("Second Greatest = " + secondMax);
    }

    // (-----TASK 5-----)

    public static int findDuplicates(String[] names) {

        String duplicates = "";
        int j = 0;
        if (names.length > 1) {
            for (int i = 0; i < names.length - 1; i++) {
                for (j = i + 1; j < names.length; j++) {
                    if (names[i].equals(names[j])) {
                        duplicates += names[i] + "\n";
                    }
                }
            }
        }
        System.out.println(duplicates);

        return 0;
    }


    public static void findMostRepeatedElementInAnArray (String[] string) {
        String element = "";
        int count = 0;

        for (int i = 0; i < string.length; i++) {
            String tempElement = string[i];
            int tempCount = 0;
            for (int j = 0; j < string.length; j++) {
                if (string[j].equals(tempElement)) tempCount++;
                if (tempCount > count) {
                    element = tempElement;
                    count = tempCount;
                }
            }
        }
        System.out.println(element);
    }




}
package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("\n=======Task1========");

        System.out.println("Hey user please give me 3 numbers");
        int num1 = scan.nextInt();
        int num2 = scan.nextInt();
        int num3 = scan.nextInt();
        scan.nextLine();

        System.out.println("Product of the numbers that you entered is = " + num1 * num2 * num3);


        System.out.println("\n=======Task2========");

        System.out.println("Hey user what is your name?");
        String firstName = scan.nextLine();

        System.out.println("Hey user what is your last name?");
        String lastName = scan.nextLine();

        System.out.println("Hey user which year you were born?");
        int yearOfBirth = scan.nextInt();
        int currentYear = 2022;
        scan.nextLine();

        System.out.println(firstName + " " + lastName + "`s age is " + (currentYear - yearOfBirth));


        System.out.println("\n=======Task3========");


        System.out.println("What is your full name?");
        String fullName = scan.nextLine();

        System.out.println("What is your weight?");
        int weight = scan.nextInt();
        double oneKgToLbs = 2.205;
        scan.nextLine();


        System.out.println("User: " + fullName);
        System.out.println("User: " + weight * 2.205 + " lbs");// or  System.out.println("User: " + weight * oneKgToLbs);


        System.out.println("\n=======Task4========");


        System.out.println("What is your full name ?");
        String student1 = scan.nextLine();

        System.out.println("What is your age ? ");
        int age1 = scan.nextInt();
        scan.nextLine();

        System.out.println("What is your full name ?");
        String student2 = scan.nextLine();

        System.out.println("What is your age ?");
        int age2 = scan.nextInt();
        scan.nextLine();

        System.out.println("What is your full name?");
        String student3 = scan.nextLine();

        System.out.println("What is your age ?");
        int age3 = scan.nextInt();

        System.out.println(student1 + "`s age is " + age1 + ".");
        System.out.println(student2 + "`s age is " + age2 + ".");
        System.out.println(student3 + "`s age is " + age3 + ".");
        System.out.println("The average age is " + (age1 + age2 + age3) / 3);


        System.out.println("The eldest age is " + Math.max(Math.max(age1, age2), age3) + ".");
        System.out.println("The youngest age is " + Math.min(Math.min(age1, age2), age3) + ".");


    }
}

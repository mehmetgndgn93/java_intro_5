package conditional_statements.if_else_statements;

public class IfElseSyntax {
    public static void main(String[] args){
        /*
        -If or else statements are used to control the flow of the program based on conditions
        -conditions are always boolean statements ( true orf false)
         */


        boolean condition = true;


        if(condition){
            System.out.println("AAA");

        }
        else{
            System.out.println("bb");
        }

        System.out.println("End of the program!");

    }
}

package conditional_statements.if_else_statements;

import java.util.Scanner;

public class Exercise2_EvenOrOdd {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter a number");
        int number = scan.nextInt();

        if(number % 2 == 1){
            System.out.println("it is an odd number");
        }
        else {
            System.out.println("this is an even number");
        }




    }
}

package conditional_statements.if_elseif_else_ladder_statements;

import java.util.Scanner;

public class Exercise1_PositiveOrNegativeOrZero {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("enter a number");
        int num = input.nextInt();


        if(num > 0){
            System.out.println("Positive");
        }else if( num == 0){
            System.out.println("ZERO");
        }
        else{
            System.out.println("Negative");
        }

    }
}

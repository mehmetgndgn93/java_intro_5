package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n------TASK1------\n");
        String result = "";

        for (int i = 1; i <= 100; i++) {
            if (i % 7 == 0)
                result += i + " - ";
        }
        System.out.println(result.substring(0, result.length() - 3));


        System.out.println("\n------TASK2------\n");
        String result1 = "";
        for (int i = 1; i <= 50; i++) {

            if (i % 2 == 0 && i % 3 == 0)
                result1 += i + " - ";
        }
       System.out.println(result1.substring(0, result1.length() - 3));


        System.out.println("\n------TASK3------\n");
        String result2 = "";

        for (int i = 100; i >= 1; i--) {
            if (i % 5 == 0)
                result2 += i + " - ";
        }
        System.out.println(result2.substring(0, result2.length() - 3));


        System.out.println("\n------TASK4------\n");

         //I tried to use Math.sqrt() but did not work   for me and after a small search I came up  with this idea, hope it`s okay

       for (int i = 0; i <=7; i++) {
            int square= (i * i);
            System.out.println("The square of " + i + " is " +  square );
        }

        System.out.println("\n------TASK5------\n");

        int sum = 0;
        for (int i = 1; i <= 10; i++) {
            sum += i;
        }
        System.out.println(sum);

        System.out.println("\n------TASK6------\n");

        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter a positive number");
        int number = scan.nextInt();
        scan.nextLine();

        int i ,factorial  = 1;

        for(i = 1 ; i <= number; i++){
            factorial  = factorial  * i;
        }
        System.out.println(factorial );

        System.out.println("\n------TASK7------\n");

        System.out.println("Please enet your name and last name");
        String fullName = scan.nextLine();
        int vowelCounter = 0;

        for (int j = 0; j <= fullName.length()-1 ; j++) {
            char ch = fullName.charAt(j);

          if(ch == 'a'|| ch == 'e'|| ch == 'i'|| ch == 'o'|| ch == 'u'
                    ||ch == 'A'|| ch == 'E'|| ch == 'I'|| ch == 'O'|| ch == 'U' )
              vowelCounter++;

        }
        System.out.println("There are " + vowelCounter + " vowel letters in this full name");


         System.out.println("\n------TASK8------\n");

        int num1 = 0;
        int summ = 0;

        do {
            System.out.println("Please enter a number");
            num1 = scan.nextInt();
            if (num1 > 100) {System.out.println("This number is already more then 100");
            break;
        }
        if  ( summ + num1 <= 100)
            summ += num1;

        } while (summ < 100);
        System.out.println("Sum of the entered numbers is at least 100");

        /* I can`t get the loop to just print out one message when sum is over 100, also I can`t make it stop asking
         for numbers where after the two given numbers over 100
         */

        System.out.println("\n------TASK9------\n");

        int count, num11 = 0, num2 = 1;
        System.out.println("How may numbers you want in the sequence:");
        count = scan.nextInt();

        System.out.print("Fibonacci Series of " + count + " numbers:");

        int j = 1;
        while (j <= count) {
            System.out.print(num11 + " ");
            int sumOfPrevTwo = num11 + num2;
            num11 = num2;
            num2 = sumOfPrevTwo;
            j++;
        }
        System.out.println("\n------TASK10------\n");
        String name;

        do {
            name = ScannerHelper.getAName();
        } while (name.toLowerCase().charAt(0) != 'j');

        System.out.println("End of the program");

        ///for (int j = 0; ; j++) {
            name = ScannerHelper.getAName();
            if (name.toLowerCase().charAt(j) == 'j'){
                System.out.println("End of program");
                //break;
 
                
                /* I used fori loop for this  then I realised it only loops for the length of the word instead of keep looping
                I guess I need to use do-while for this but my brain turned into a toasted bread I will watch the recap and try to
            earn from there*/
        }
    }
}
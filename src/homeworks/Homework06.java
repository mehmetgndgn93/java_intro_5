package homeworks;

import java.util.Arrays;
import java.util.Objects;

public class Homework06 {
    public static void main(String[] args) {


        System.out.println("\n-----TASK 1--------\n");

        int[] numbers = new int[10];
        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n-----TASK 2--------\n");

        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));

        System.out.println("\n-----TASK 3--------\n");

        int[] numbers1 = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers1));
        Arrays.sort(numbers1);
        System.out.println(Arrays.toString(numbers1));

        System.out.println("\n-----TASK 4--------\n");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));


        System.out.println("\n-----TASK 5--------\n");

        String[] cartoonDogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoonDogs));
        boolean found = false;

        for (String cartoonDog : cartoonDogs) {
            if (cartoonDog.equals("Pluto")) {
                found = true;
                break;
            }
        }
        System.out.println(found);

        System.out.println("\n-----TASK 6--------\n");

        String[] cartoonCats = {"Garfield", "Tom", "Sylvester", "Azreal"};
        Arrays.sort(cartoonCats);
        System.out.println(Arrays.toString(cartoonCats));

        boolean hasGarfieldAndFelix = false;

        for (String cartoonCat : cartoonCats) {
            if (cartoonCat.toLowerCase().contains("garfield") && cartoonCat.toLowerCase().contains("felix"))
                System.out.println(true);
        }
        System.out.println(hasGarfieldAndFelix);

        System.out.println("\n-----TASK 7--------\n");

        double[] doubles = {10.5, 20.75, 70, 80, 15.75};
        System.out.println(Arrays.toString(doubles));

        for (double aDouble : doubles) {

            System.out.println(aDouble);
        }

        System.out.println("\n--------TASK8------");

        char[] c = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(c));
        int countLetters = 0; // countLowerCase + countUpperCase
        int countLowerCase = 0;
        int countUpperCase = 0;
        int digits = 0;
        int specials = 0;

        for (char c1 : c) {
            if(Character.isUpperCase(c1)) countUpperCase++;
            else if(Character.isLowerCase(c1)) countLowerCase++;
            else if(Character.isDigit(c1)) digits++;
            else if(c1 != ' ' ) specials++;

        }

        System.out.println(countUpperCase);
        System.out.println(countLowerCase);
        System.out.println(digits);
        System.out.println(specials);
        System.out.println(countUpperCase+countLowerCase);


        System.out.println("\n----TASK 09----\n");


        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        int upperCase = 0;
        int lowerCase = 0;
        int letterBP = 0;
        int counter = 0;


        for (String object : objects) {
            if (Character.isUpperCase(object.charAt(0)))
                upperCase++;
            else if (Character.isLowerCase(object.charAt(0)))
                lowerCase++;
            if (object.toLowerCase().startsWith("b") || object.toLowerCase().startsWith("p"))
                letterBP++;
            if (object.toLowerCase().contains("book") || object.toLowerCase().contains("pen"))
                counter++;

        }

        System.out.println(Arrays.toString(objects));
        System.out.println("Elements starts with uppercase = " + upperCase);
        System.out.println("Elements starts with lowercase = " + lowerCase);
        System.out.println("Elements starts with B or P = " + letterBP);
        System.out.println("Elements having ”book” or “pen” = " + counter);


        System.out.println("\n----TASK 10----\n");

        int[] nums = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        int moreThen10 = 0;
        int lessThen10 = 0;
        int just10 = 0;
        System.out.println(Arrays.toString(nums));

        for (int num : nums) {
            if (num > 10)
                moreThen10++;
        }
        System.out.println("Elements are more then 10 = " + moreThen10);


        for (int num : nums) {
            if (num < 10)
                lessThen10++;
        }
        System.out.println("Elements are less then 10 = " + lessThen10);


        for (int num : nums) {
            if (num == 10)
                just10++;
        }
        System.out.println("Elements are  10 = " + just10);


        System.out.println("\n----TASK 11----\n");

        int[] arr1 = {5, 8, 13, 1, 2};
        int[] arr2 = {9, 3, 67, 1, 0};
        int[] arr3 = new int[5];

        for (int i = 0; i < arr1.length; i++) {
            arr3[i] = Math.max(arr1[i], arr2[i]);
        }

        System.out.println("1st array is = " + Arrays.toString(arr1));
        System.out.println("2nd array is = " + Arrays.toString(arr2));
        System.out.println("3rd array is = " + Arrays.toString(arr3));

    }

}















package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {


        System.out.println("\n-------TASK1-------\n");


        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter 2 numbers");
        int num2 = scan.nextInt();
        int num1 = scan.nextInt();
        scan.nextLine();


        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));


        System.out.println("\n-------TASK2-------\n");

        System.out.println("Please enter 5 different  numbers");
        int entry1 = scan.nextInt();
        int entry2 = scan.nextInt();
        int entry3 = scan.nextInt();
        int entry4 = scan.nextInt();
        int entry5 = scan.nextInt();
        scan.nextLine();


        System.out.println("Max value  = " + Math.max(Math.max(entry1, entry2), (Math.max(entry3, entry4))));
        System.out.println("Min value  = " + Math.min(Math.min(entry1, Math.min(entry2, entry3)), Math.min(entry3, entry4)));
        System.out.println("Max value = " + Math.max(Math.max(entry1, Math.max(entry2, entry3)), Math.max(entry4, entry5)));

        System.out.println("\n-------TASK2-------\n");

        System.out.println("Number 1 = " + Math.random() * (100 - 50 + 1) + 50);
        System.out.println("Number 2 = " + Math.random() * (100 - 50 + 1) + 50);
        System.out.println("Number 3 = " + Math.random() * (100 - 50 + 1) + 50);


        System.out.println("\n-------TASK4-------\n");


        double alexMoney = 125, mikeMoney = 220;
        double givenMoney = 25.5;

        System.out.println("Alex`s money: $" + (alexMoney - givenMoney));
        System.out.println("Mike`s money: $" + (mikeMoney + givenMoney));

        System.out.println("\n-------TASK5-------\n");
        int costOfBike = 390;
        double dailySavings = 15.60;
        int days = (int) (costOfBike / dailySavings);

        System.out.println(days);


        String s1 = "5", s2 = "10";

        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);

        System.out.println("Sum of 5 and 10 is = " + (i1 + i2));
        System.out.println("Product of 5 and 10 is = " + (i1 * i2));
        System.out.println("Division of 5 and 10 is = " + (i1 / i2));
        System.out.println("Subtraction of 5 and 10 is = " + (i1 - i2));
        System.out.println("Remainder of 5 and 10 is = " + (i1 % i2));


        double savingToBuyACar = 14265;
        double opt1 = 475.50;
        double opt2 = 951;
        int months = (int) (savingToBuyACar / opt1);
        int months2 = (int) (savingToBuyACar / opt2);

        System.out.println("Option 1 will take " + months + " months");
        System.out.println("Option 2 will take " + months2 + " months");


        System.out.println("Enter 2 numbers");
        int num12 = scan.nextInt(), num23 = scan.nextInt();

        System.out.println((double) num12 / (double) num23);

        int random1, random2, random3;

        random1 = (int) (Math.random() * 101);
        random2 = (int) (Math.random() * 101);
        random3 = (int) (Math.random() * 101);

        System.out.println(random1);
        System.out.println(random2);
        System.out.println(random3);

        boolean allNumsOver25 = (random1 > 25) && (random2 > 25) && (random3 > 25);


        System.out.println(allNumsOver25);

        System.out.println("please enter a number between 1 and 7");
        int dayOfTheWeek = scan.nextInt();

        if (dayOfTheWeek == 1) System.out.println("Monday");
        else if (dayOfTheWeek == 2) System.out.println("Tuesday");
        else if (dayOfTheWeek == 3) System.out.println("wednesday");
        else if (dayOfTheWeek == 4) System.out.println("thursday");
        else if (dayOfTheWeek == 5) System.out.println("friday");
        else if (dayOfTheWeek == 6) System.out.println("saturday");
        else if (dayOfTheWeek == 7) System.out.println("sunday");
        else System.out.println("Number is not in scope");


        switch (dayOfTheWeek) {
            case (1):
                System.out.println("Monday");
                break;
            case (2):
                System.out.println("tuesday");
                break;
            case (3):
                System.out.println("wednesday");
                break;
            case (4):
                System.out.println("thursday");
                break;
            case (5):
                System.out.println("friday");
                break;
            case (6):
                System.out.println("satruday");
                break;
            case (7):
                System.out.println("sunday");
                break;
            default:
                System.out.println("Not in scope");


                System.out.println("tell me your exam results");
                int exam1 = scan.nextInt();
                int exam2 = scan.nextInt();
                int exam3 = scan.nextInt();
                int average = (exam1 + exam2 + exam3) / 3;


                if (average >= 70) System.out.println("YOU PASSED");
                else System.out.println("YOU HAVE FAILED");


                int num15, num25, num35;

                System.out.println("Please enter 3 numbers");
                num15 = scan.nextInt();
                num25 = scan.nextInt();
                num35 = scan.nextInt();


                if (num15 == num25 && num25 == num35) System.out.println("Triple match");
                else if (num15 == num25 || num25 == num35 || num25 == num35) System.out.println("Double match");
                else System.out.println("NO match");


        }


    }

}









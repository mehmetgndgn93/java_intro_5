package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n--------TASK1-------\n");

        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));

        System.out.println(nums.get(3));
        System.out.println(nums.get(0));
        System.out.println(nums.get(2));
        System.out.println(nums);

        System.out.println("\n--------TASK2-------\n");

        String[] str = {"Blue", "Brown", "Red", "White", "Black", "Purple"};
        ArrayList<String> str1 = new ArrayList<>();
        Collections.addAll(str1, str);

        System.out.println(str1.get(1));
        System.out.println(str1.get(3));
        System.out.println(str1.get(5));
        System.out.println(str1);


        System.out.println("\n--------TASK3-------\n");

        ArrayList<Integer> nums1 = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));
        System.out.println(nums1);
        Collections.sort(nums1);
        System.out.println(nums1);

        System.out.println("\n--------TASK4-------\n");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n--------TASK5-------\n");

        ArrayList<String> cartoons = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));

        boolean hasWolverine = false;
        for (String c : cartoons) {
            if (cartoons.contains("Wolverine"))
                hasWolverine = true;
            break;
        }
        System.out.println(cartoons);
        System.out.println(hasWolverine);

        System.out.println("\n--------TASK6-------\n");

        ArrayList<String> cartoons2 = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        boolean hasHulkAndIronMan = false;

        for (String c1 : cartoons2) {
            if (cartoons2.contains("Hulk") || cartoons2.contains("Iron Man"))
                hasHulkAndIronMan = true;
            break;
        }
        Collections.sort(cartoons2);
        System.out.println(cartoons2);
        System.out.println(hasHulkAndIronMan);


        System.out.println("\n--------TASK7-------\n");

        ArrayList<Character> characters = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));
        System.out.println(characters);

        for (Character character : characters) {
            System.out.println(character);

        }
        System.out.println("\n--------TASK8-------\n");

        ArrayList<String> objects = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));
        int countM = 0;
        int countNoAOrE = 0;
        for (String element : objects) {
            if (element.toLowerCase().contains("m")) countM++;
            else if (!element.toLowerCase().contains("a") || !element.toLowerCase().contains("e")) countNoAOrE++;
            // For some reason I am getting 2 for else if statement...
        }
        System.out.println(objects);
        Collections.sort(objects);
        System.out.println(objects);
        System.out.println(countM);
        System.out.println(countNoAOrE);

        System.out.println("\n--------TASK9-------\n");

        ArrayList<String> kObjects = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        int upperCase = 0;
        int lowerCase = 0;
        int hasP = 0;
        int endOrStartsWithP = 0;

        for (String k : kObjects) {
            if (Character.isUpperCase(k.charAt(0))) upperCase++;
            else if(Character.isLowerCase(k.charAt(0))) lowerCase++;
            else if(Character.isLetter(k.charAt(0))) hasP++;
            //else if  endOrStartsWithP++;
            //I could not figure out how to check the last and first index of an element.
        }
        System.out.println("Elements starts with uppercase = " + upperCase);
        System.out.println("Elements starts with lowercase = " +lowerCase);
        System.out.println("Elements having P or p=  " + hasP) ;
        System.out.println("Elements starting or ending with P or p = " + endOrStartsWithP);


        System.out.println("\n--------TASK10-------\n");



    }
}


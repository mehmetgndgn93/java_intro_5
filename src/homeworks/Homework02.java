package homeworks;


import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        System.out.println("------------Task1---------\n");

        int num1, num2;

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter the  number 1: ");
        num1 = input.nextInt();

        System.out.println("Please enter number 2 : ");
        num2 = input.nextInt();

        System.out.println("The number 1 entered by user is: " + num1 + "\n" +
                "The number 2 entered by the user is: " + num2 + "\n" +
                "The sum of number 1 and number 2 entered by user is = " + (num1 + num2));


        System.out.println("\n------------Task2---------\n");

        int number1, number2;

        System.out.println("Please enter number 1: ");
        number1 = input.nextInt();

        System.out.println("Please enter number 2: ");
        number2 = input.nextInt();

        System.out.println("Product of the given 2 numbers is: " + number1 * number2);


        System.out.println("\n------------Task3---------\n");

        double number_1, number_2;

        System.out.println("Please enter number 1: ");
        number_1 = input.nextDouble();

        System.out.println("Please enter number 2: ");
        number_2 = input.nextDouble();

        System.out.println("The sum of the given numbers is = " + (number_1 + number_2) + "\n");
        System.out.println("The product of the given numbers is = " + number_1 * number_2 + "\n");
        System.out.println("The subtraction of the given numbers is = " + (number_1 - number_2) + "\n");
        System.out.println("The division of the given numbers is = " + number_1 / number_2 + "\n");
        System.out.println("The remainder of the given numbers is = " + number_1 % number_2 + "\n");

        System.out.println("\n------------Task4---------\n");


        System.out.println("1. " + (-10 + 7 * 5) + "\n" +
                "2. " + (72 + 24) % 24 + "\n" +
                "3. " + (10 + -3 * 9 / 9) + "\n" +
                "4. " + (5 + 18 / 3 * 3 - (6 % 3)) + "\n");


        System.out.println("\n------------Task5---------\n");

        int number3, number4;

        System.out.println("Please enter number 1: ");
        number3 = input.nextInt();

        System.out.println("Please enter number 2: ");
        number4 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (number3 + number4) / 2);

        System.out.println("\n------------Task6---------\n");

        int number5, number6, number7, number8, number9;

        System.out.println("Please enter number 1: ");
        number5 = input.nextInt();

        System.out.println("Please enter number 2: ");
        number6 = input.nextInt();

        System.out.println("Please enter number 3: ");
        number7 = input.nextInt();

        System.out.println("Please enter number 4: ");
        number8 = input.nextInt();

        System.out.println("Please enter number 5: ");
        number9 = input.nextInt();


        System.out.println("The average of the given numbers is: " + (number5 + number6 + number7 +
                number8 + number9) / 5);

        System.out.println("\n------------Task7---------\n");


        int number10, number11, number12;

        System.out.println("Please enter  the first number: ");
        number10 = input.nextInt();

        System.out.println("Please enter the second number: ");
        number11 = input.nextInt();

        System.out.println("Please enter the third number: ");
        number12 = input.nextInt();


        System.out.println("The number 5 multiplied with 5 is: " + number10 * number10);
        System.out.println("The number 6 multiplied with 6 is: " + number11 * number11);
        System.out.println("The number 10 multiplied with 10 is: " + number12 * number12);

        System.out.println("\n------------Task8---------\n");


        int number00;

        System.out.println("Please enter the side of a square: ");
        number00 = input.nextInt();

        System.out.println("Perimeter of the square =  " + 4 * number00 + "\n"
                + "Area of the square = " + number00 * number00);

        System.out.println("\n------------Task9---------\n");


        double salary = 90_000;

        System.out.println("A Software Engineer in Test can earn " + " " + "$" + salary * 3 + " in 3 years.");

        System.out.println("\n------------Task10---------\n");

        String favBook, favColor, favNumber;


        System.out.println("What is your favorite book?");
        favBook = input.next();


        System.out.println("What is your favorite color?");
        favColor = input.nextLine();


        System.out.println("What is yor fav number?");
        favNumber = input.nextLine();


        System.out.print("User`s favorite book is: " + favBook + "\n"
                + "User`s favorite color is: " + favColor + "\n"
                + "User`s favorite number is: " + favNumber);


        System.out.println("\n------------Task11---------\n");

        String firstName, lastName, eMail, address, phoneNumber;
        int age;


        System.out.println("Please enter your first name?");
        firstName = input.nextLine();

        System.out.println("Please enter your last name?");
        lastName = input.nextLine();

        System.out.println("Please enter your age?");
        age = input.nextInt();


        System.out.println("Please enter your email address: ");
        eMail = input.nextLine();

        System.out.println();


        System.out.println("Please enter your phone number: ");
        phoneNumber = input.nextLine();

        System.out.println();


        System.out.println("Please enter your address: ");
        address = input.nextLine();


        System.out.print("\t" + "User who joined this program is" + " " + firstName + " " + lastName + "." + firstName
                + "`s  age is " + age + "." + firstName + "`s email address is" + eMail + "phone number is " + phoneNumber + ","
                + " and address is" + ".");


    }
}
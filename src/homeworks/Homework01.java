package homeworks;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;

public class Homework01 {
    public static void main(String[] args) {
        /*
        TASK1
        JAVA -> J = 01001010
                A = 01000001
                V = 01010110
                A = 01000001
        SELENIUM ->
                S = 01010011
                E = 01000101
                L = 01001100
                E = 01000101
                N = 01001110
                I = 01001001
                U = 01010101
                M = 01001101
         */

        /*
         TASK 2
            01001101 = M
            01101000 = h
            01111001 = y
            01010011 = S
            01101100 = l
         */

            /*
        TASK 3
              */
        System.out.print("I start to practice \"JAVA\" today, and I like it\n");
        System.out.print("The secret of getting ahead is getting started.\n");
        System.out.print("\"Don't limit yourself.\"\n");
        System.out.print("Invest in your dreams. Grind now. Shine later.\n");
        System.out.print("It’s not the load that breaks you down, it’s the way you carry \nit.");
        System.out.print("\nThe hard days are what make you stronger.\n");
        System.out.print("You can waste your lives drawing lines. Or you can live your \n" + "life crossing them.\n\n");


        System.out.println("----------!!!!--------\n\n");

        /*
        TASK 4
         */
        System.out.println(" \tJava is easy to write and easy to run—this is the \n" +
                "foundational strength of Java and why many developers \n" +
                "program in it. When you write Java once, you can run it \n" +
                "almost anywhere at any time.\n\n" +
                "\tJava can be used to create complete applications \n" +
                "that can run on a single computer or be distributed \n" +
                "across servers and clients in a network.\n\n" +
                "\tAs a result, you can use it to easily build mobile \n" +
                "applications or run-on desktop applications that use \n" +
                "different operating systems and servers, such as Linux \n" +
                "or Windows.\n");

        System.out.println("----------!!!!------\n\n");
        /*
        TASK 5
         */
        byte myAge = 29;
        byte myFavoriteNumber = 7;
        String myHeight = ("5'11″"); // I used string and  a different double quotation for the inches. I will wait for the solution If my way was wrong.
        int myWeight = 205;
        char myFavoriteCharacter = '!';


        System.out.println(myAge);
        System.out.println(myFavoriteNumber);
        System.out.println(myHeight);
        System.out.println(myWeight + " lbs.");
        System.out.println(myFavoriteCharacter);

        System.out.println("----------!!!!------\n\n");


        // I did the println statements in 2 different way as the one below, I did not want to risk it.

        System.out.println("My age is " + myAge);
        System.out.println("My favorite number is " + myFavoriteNumber);
        System.out.println("My height is " + myHeight);
        System.out.println("My weight is " + myWeight + " lbs.");
        System.out.println("My favorite character is  " + myFavoriteCharacter);


    }
}

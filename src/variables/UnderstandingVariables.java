package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {

        //Variable declaration and assignment with an initiaal value

        int age = 45;

        System.out.println(age); //45

        //Variable declaration = it allocates the memory
        String name;

        //Variable assignment
        name = "John";

        System.out.println(name);

        name = "Mike";

        System.out.println(name);

        age = 21;

        System.out.println(age);



        /*
        boolean isFemale = true;
        String babyName;

        if (isFemale) {
            babyName = "Ema";
        } else {
            babyName = "George";

        }
        System.out.println(babyName);
        */

    }
}

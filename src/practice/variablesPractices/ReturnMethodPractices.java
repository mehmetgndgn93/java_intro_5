package practice.variablesPractices;

public class ReturnMethodPractices {
    public static void main(String[] args) {

    }
            /*
        Create a public static method named as "findDifference" it will take two values and returns the difference

        Example:
        6, 9 -> 3
        4, 1 -> 3
        99, 23 -> 76
            */

     public static int findDifference(int num1, int num2){
         return Math.abs(num1 - num2);

     }
}

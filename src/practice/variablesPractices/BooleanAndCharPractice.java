package practice.variablesPractices;

public class BooleanAndCharPractice {
    public static void main(String[] args) {
        /*
        boolean negative = false;
        boolean positive = true;

        System.out.println("Negative = \"" + negative + "\"");
        System.out.println("Positive = \"" + positive + "\"");

        */


        char myFirstChar = 'M';
        char mySecondChar = 'e';
        char myThirdChar = 'h';


        System.out.print(myFirstChar);
        System.out.print(mySecondChar);
        System.out.print(myThirdChar);

        System.out.println();

        char firstLetter = 'C';
        char secondLetter = 'a';
        char thirdLetter = 't';

        System.out.print("\"" + firstLetter);
        System.out.print(secondLetter);
        System.out.print(thirdLetter + "\"");


    }
}

package practice.variablesPractices;

public class StringPractice {
    public static void main(String[] args) {
        // string reference type of data

        String schoolName = "TechGlobal";
        String myName = "Mehmet";
        int myAge = 99;
        double myHeight = 6.0;

         boolean isEligiableToDrive = true;
         

        System.out.println("My school name is "+ schoolName + ". My name is " + myName +
                ". I am " + myAge+ " years old. My height is " + myHeight + "." + "I am " +
                "eligable to drive  \"" + isEligiableToDrive +"\".");


    }
}

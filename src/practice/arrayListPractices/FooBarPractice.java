package practice.arrayListPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class FooBarPractice {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));

        for (int i = 0; i <numbers.size() ; i++) {
            if(i % 10 == 0) System.out.println("FooBar");
            else if (i%5 == 0) System.out.println("Bar");
            else if (i%2 == 0) System.out.println("Foo");
            else System.out.println(i);

        }



    }
}

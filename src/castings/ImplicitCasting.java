package castings;

public class ImplicitCasting {
    public static void main (String[] args){

        //implicit casting - widening - upcasting
        byte b = 25; // -128, 127

        int i = b;

        System.out.println(i);

        float f = 10.5f;
        double d = f;

        System.out.println(d);






        /*
        byte b = 25; // -128 to 127

        int i = b;
        System.out.println(i);

        float f = 10.5f;
        double d = f;

        System.out.println(d);

        // Explicit casting - narrowing -down casting

        int age = 25;
        byte b1 = (byte) age;
        System.out.println(b1);
        //BUT we will lose data if bigger primitives holds a data that
        //cannot be stored in the small one.

        int num1 = 5000;
        byte num2 = (byte) num1;

        System.out.println(num2);


        */

    }
}

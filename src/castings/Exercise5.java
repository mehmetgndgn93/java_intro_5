package castings;

public class Exercise5 {
    public static void main(String[] args) {
        String s1 = "35" ,s2 = "10";

        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);

        /*
        Find below
        Difference of s1 and s2
        absolute difference of s1 and s2
         */

        System.out.println("Product of " + s1 + " and + " + s2 + "= " + s1 + s2  );
        System.out.println("Sum of s1 and s2 = " + (i1 + i2));
        System.out.println("Dif of s1 and s2 = " + (i1 - i2));
        System.out.println("abs of s1 and s2 = " + Math.abs(i1 - i2));
        System.out.println("product of s1 and s2 = " +  (i1 * i2));
        System.out.println("division of s1 and s2 = " +  (i1 / i2));
        System.out.println("remainder of s1 and s2 = " +  (i1 % i2));
        System.out.println("max of s1 and s2 = " +  Math.max(i1, i2));
        System.out.println("min of s1 and s2 = " +  Math.min(i1 ,i2));

    }
}

package castings;

public class Exercise6_CalculateSalary {
    public static void main(String[] args) {
        String salary1 = "5000";
        String salary2 = "6000.25";
        String salary3 = "4000.50";

        double d1 = Double.parseDouble(salary1);
        double d2 = Double.parseDouble(salary2);
        double d3 = Double.parseDouble(salary3);

        double minSalary = Math.min(d3,Math.min(d1,d2));


        System.out.println("Min salary = " + Math.min(d3,Math.min(d1,d2)));
        System.out.println("Max salaray = " + Math.max(d1,Math.max(d2, d3) ));

        /*
        Find the 10 percent of the min salary
        expected output:
        400.05
         */

        System.out.println(" %10 of salary = " + Math.min(d3,Math.min(d1,d2)) / 10);
        System.out.println(" %10 of salary = " + minSalary / 10);



    }
}

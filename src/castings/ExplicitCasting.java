package castings;

public class ExplicitCasting {
    public static void main(String[] args) {

        //Explicit casting - narrowing - downcasting

        int age = 25;
        byte b = (byte) age;
        System.out.println(b); //25

        //BUT we will lose data if bigger primitive holds a data that cannot be stored in the small one

        int num1 = 135;

        byte num2 = (byte) num1;
        System.out.println(num2);






    }
}

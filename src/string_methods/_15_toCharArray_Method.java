package string_methods;

import java.util.Arrays;

public class _15_toCharArray_Method {
    public static void main(String[] args) {

        /*
        return type
        returns a char Array
        non static
        it doesnt take any argument

         */


        String s = "TechGlobal School";

        System.out.println(Arrays.toString(s.toCharArray()));




    }
}

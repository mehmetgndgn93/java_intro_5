package string_methods;

public class _12_contains_Method {
    public static void main(String[] args) {




        String s = "TechGlobal School";

        System.out.println(s.contains("Tech")); // true
        System.out.println(s.contains("School")); // true
        System.out.println(s.contains("a")); // true
        System.out.println(s.contains("E")); // FALSE

        //IMPORTANT
        System.out.println(s.contains("")); // true
    }
}

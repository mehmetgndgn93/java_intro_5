package string_methods;

import java.util.Scanner;

public class Exercise05_FirstAndLastChars {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter a name");
        String s1 = scan.nextLine();

        System.out.println("First character in the " + s1 + " is = " + s1.charAt(0));
        System.out.println("Last character in the " + s1 + " is = " + s1.charAt(s1.length()-1));


    }
}

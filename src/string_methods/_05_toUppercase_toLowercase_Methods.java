package string_methods;

import java.util.Locale;

public class _05_toUppercase_toLowercase_Methods {
    public static void main(String[] args) {

        /*
        1.return
        2.return a String
        3.non-static
        4.The one we use does not take any argument - there are overloaded methods.
         */

        String name = "John";

        System.out.println(name.toLowerCase()); //john
        System.out.println(name.toUpperCase()); //JOHN

        System.out.println("hello".toUpperCase());
        System.out.println("hELLO".toLowerCase());

        System.out.println("abc".toUpperCase().toLowerCase());
        System.out.println("ab".toUpperCase().concat("xY".toLowerCase()));
    }
}

package arrays;

import java.util.Arrays;

public class Exercise01_countChar {
    public static void main(String[] args) {
        /*
        assume you are given as below

        String word = "Java";

        count how many a letters you have in the String

         */
//
//        String word = "Java";
//
//        int countA = 0;
//        for (int i = 0; i < word.length(); i++) {
//
//            if (word.charAt(i) == 'a')
//                countA++;
//        }
//        System.out.println(countA);


        String word = "Java";

        char[] wordArray = word.toCharArray();

        int countA = 0;

        for (char element : word.toCharArray()) {
            if(element == 'a') countA++;
        }
        System.out.println(countA);


    }
}

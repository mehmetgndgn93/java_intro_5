package arrays;

import java.util.Arrays;

public class _07_StringArray_Recap {
    public static void main(String[] args) {
        // String array with the length of 4

        String[] fruits = new String[4];

        System.out.println(Arrays.toString(fruits));

        fruits[0] = "Apple";
        fruits[3] = "Kiwi";
        System.out.println(Arrays.toString(fruits));

        fruits[1] = "Grapes";
        fruits[2] = "Orange";
        System.out.println(Arrays.toString(fruits));

        System.out.println("\nReassigning values\n");

        //-Reassign "Apple" to index of 3 and to index of 2

        fruits[3]=fruits[0];
        fruits[2]=fruits[0];

        //Printing values with for each loop
        for (String fruit : fruits) {
            System.out.println(fruit);

        }


    }
}

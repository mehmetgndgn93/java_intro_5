package arrays;

import java.util.Arrays;

public class _06_doubleArray_Recap {
    public static void main(String[] args) {
        //create double arrays
        double[] decimals = {1.5, 2.3, -1.3, -3.7};

        char[] chars = {'a','b','c','d'};

        System.out.println(Arrays.toString(decimals));
        System.out.println("The length of the array is = " + decimals.length);
        Arrays.sort(decimals);
        System.out.println("Array after sorting = '" + Arrays.toString(decimals));


        /*
        Output
        -3.7 - a -> at the index 0
        -1.3 - b-> at the index 1
        1.5 - c-> at the index 2
        2.4 - d-> at the index 3
         */

         // when we   need indexes or increment number to use go with fori loop
        System.out.println("\n----Fori Loop--\n");
        for (int i = 0; i < decimals.length; i++) {
            System.out.println(decimals[i]);
        }

        // when we dont need indexes or increment number to use go with for each loop
        System.out.println("\nFor each loop");
        for(double d : decimals)
            System.out.println(d);


        System.out.println("\n----Fori Loop WITH 2 ARRAYS--\n");
        for (int i = 0; i < chars.length; i++) {
            System.out.println(decimals[i] + "-" + chars[i]);
        }

    }
}

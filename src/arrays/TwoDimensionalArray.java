package arrays;

import java.util.Arrays;

public class TwoDimensionalArray {
    public static void main(String[] args) {

        String [][] students = {
                {"Ali","Mehmet","Alex"},
                {"Alex", "Regina"},
                {"Abdallah", "Newer"}
        };

        System.out.println(students[0][1]); // Mehmet
        System.out.println(students[1][1]);

        System.out.println(students.length);
        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        System.out.println("\n-------\n");
        // printing inner array with fori loop

        for (int i = 0; i <students.length ; i++) {
            System.out.println(Arrays.toString(students[i]));
        }


        //Printing inner arrays with for each loop

        for (String[] group : students) {
            System.out.println(Arrays.toString(group));
        }


        System.out.println(Arrays.deepToString(students));


        for (String[] group : students) {
            for(String student : group){
                System.out.println(student);
            }

        }





    }
}

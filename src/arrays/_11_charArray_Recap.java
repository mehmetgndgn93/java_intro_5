package arrays;

import java.util.Arrays;

public class _11_charArray_Recap {
    public static void main(String[] args) {


        char[] initials =new char[6];
        initials[0] ='M';
        initials[1] = 'e';
        initials[2] ='h';
        initials[3] ='m';
        initials[4] ='e';
        initials[5] = 't';

         char[] initials2 = {'M','e','h','m','e','t'};
        System.out.println(Arrays.toString(initials));
        System.out.println(Arrays.toString(initials2));


        for (char in : initials2) {
            System.out.print(in);
        }

        for (int i = 0; i < initials2.length; i++) {
            System.out.println(initials2[i]);

        }



    }
}

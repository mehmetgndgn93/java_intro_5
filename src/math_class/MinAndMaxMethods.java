package math_class;

import org.omg.CORBA.ORB;

public class MinAndMaxMethods {
    public static void main(String[] args) {


        // Math class doesn`t need an object for using their methods.Simply   call name and methods after.

      /*  int maxNumber = Math.max(80, 27);
        int maxNumber1 = Math.max(maxNumber, 239);

        System.out.println(maxNumber);
        System.out.println(maxNumber1);

        int minNumber = Math.min(80, 27);
        int minNumber1 = Math.min(minNumber, 27);

        System.out.println("\nminNumber " + minNumber);
        System.out.println("\nminNumber 1" + minNumber);

        int minNumberr = Math.min(80, 27);
        int minNumberrr = Math.min(Math.min(80, 27), 2);
        int minOfTwoNeg = Math.min(-40, -60);

        System.out.println(minNumberr);
        System.out.println(minNumberrr);
        System.out.println(minOfTwoNeg);


        double  maxOfTwoDecimals = Math.max(1.7, 6.5);
        double  maxOfTwoDecimalNeg = Math.max(-5.4, -27.2);
        System.out.println(maxOfTwoDecimals);
        System.out.println(maxOfTwoDecimalNeg);

*/

            /*

            Find the max value of the given numbers and print them

            * 6, 9
            * 17, 49
            * 34.2, 12.5
            * -14, -32
            * 17, 49, 125
 */
    /*
        int num1 = Math.max(6, 9);
        int num2 = Math.max(17, 49);
        double num3 = Math.max(34.2, 12.5);
        int num4 = Math.max(-14, -32);
        int num5 = Math.max(Math.max(17, 49), 125);

        System.out.println(num1);
        System.out.println(num2);
        System.out.println(num3);
        System.out.println(num4);
        System.out.println(num5);


        // OR

        System.out.println("min is " + Math.min(6, 9));
        System.out.println("min is " + Math.min(17, 49));
        System.out.println("min is " + Math.min(34.2, 12.5));
        System.out.println("min is " + Math.min(-14, -32));
        System.out.println("min is " + Math.min(Math.min(17, 49), 125)); // doing it with 3 numbers.
        System.out.println("min is " + Math.min(Math.min(45, 32), Math.min(56, 89)));// doing it with 4 numbers.


        //// my recap

        int maxNumber = Math.max(80, 27);
        int maxNumber2 = Math.max(maxNumber, 125);

        System.out.println(maxNumber);
        System.out.println(maxNumber2);

        int minNumber = Math.min(80, 27);

        int minNumber2 = Math.min(Math.min(80, 80), 2);


        System.out.println(" min number is = " + minNumber);


        */



        int maxNumber = Math.max(Math.max(17, 49), 129);




        System.out.println(maxNumber);
        System.out.println(Math.max( Math.max(45, 32),  Math.max(56, 89)   ) );




    }
}

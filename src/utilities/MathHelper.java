package utilities;

public class MathHelper {

    /*
    Write a method that returns the max number of 3 int numbers
     */

    public static int maxOfThree(int num1, int num2, int num3) {
        return Math.max(Math.max(num1, num2), num3);

    }

    /*
  Write a method that returns the min number of 3 int numbers
   */
    public static int minOfThree(int num1, int num2, int num3) {
        return Math.min(Math.min(num1, num2), num3);

    }
        /*
       Write a method that returns  the middle number of 3 in numbers
       assume numbers can never be equal to each other
         */


    public static int middleOfThree(int num1, int num2, int num3) {
        int max = maxOfThree(num1, num2, num3);
        int min = minOfThree(num1, num2, num3);
        return num1 + num2 + num3 - min - max;

    }


    //public static boolean isEven ( int num ){
    // if(num1 % 2 == 0 ) return true;
    // else return false;
    // ternay --> boolean = num % 2 == 0 ? true : false
    // return even;

    public static boolean isEven(int num) {
        return num % 2 == 0;


        }

    /*
    Create a method that takes 2 int arguments and returns their sum
    Method name = sum

    public static
    return type
    return int
    it takes 2 int

     */




    public static int sum(int num1, int num2) {
        return num1 + num2;
    }

    ///OVER LOADING A METHOD
    public static int sum(int num1, int num2, int num3) {
        return num1 + num2 + num3;


    }
}